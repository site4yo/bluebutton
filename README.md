#Install

```shell
$ cd /usr/lib
$ git clone https://bitbucket.org/site4yo/bluebutton
```

```shell
$ cd bluebutton
$ gem install bundle
$ bundle install
```

#Подключение кнопки

```shell
$ bluetoothctl
[bluetooth]$ power on
[bluetooth]$ scan on
```

* Включить кнопку и дождатся в списке:

```shell
[NEW] Device FF:FF:1D:14:79:80 AB Shutter 3
```

* Теперь можно выполнить сопряжение:
```shell
[bluetooth]$ pair FF:FF:1D:14:79:80
[CHG] Device FF:FF:1D:14:79:80 Paired: yes
Pairing successful
[AB Shutter3    ]$ trust FF:FF:1D:14:79:80
[CHG] Device FF:FF:1D:14:79:80 Trusted: yes
[AB Shutter3    ]$ quit
```

* Теперь можно проверить работу демона

```shell
$ sudo ./bin/bluebutton
Try to find device AB Shutter 3...
Device AB Shutter 3 find at /dev/input/event0
Unable to connect to X server
Reading events from /dev/input/event0...
Connected

```

* Добавление сервиса

```shell
$ cp /usr/lib/bluebutton/bluebutton.service /lib/systemd/system/
$ systemctl daemon-reload
$ systemctl start bluebutton.service
$ systemctl status bluebutton.service
$ systemctl enable bluebutton.service
```