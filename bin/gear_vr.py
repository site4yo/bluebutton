#!/usr/bin/python3

import requests
import gatt
import math
import time
import numpy as np
import sys
import os

manager = gatt.DeviceManager(adapter_name='hci0')

class AnyDevice(gatt.Device):
    def connect_succeeded(self):
        print("[%s] Соединились" % (self.mac_address))
        super().connect_succeeded()

    def connect_failed(self, error):
        print("[%s] Соединение разорвано: %s" % (self.mac_address, str(error)))
        super().connect_failed(error)

    def disconnect_succeeded(self):
        print("[%s] Отсоединились" % (self.mac_address))
        sys.exit(0)

    def write(self, cmd, times):
        for i in range(times - 1):
            self.__setup_characteristic.write_value(cmd)

    def services_resolved(self):
        super().services_resolved()

        controller_data_service = next(
            s for s in self.services
            if s.uuid == '4f63756c-7573-2054-6872-65656d6f7465')

        controller_setup_data_characteristic = next(
            c for c in controller_data_service.characteristics
            if c.uuid == 'c8c51726-81bc-483b-a052-f7a14ea3d282')

        controller_data_characteristic = next(
            c for c in controller_data_service.characteristics
            if c.uuid == 'c8c51726-81bc-483b-a052-f7a14ea3d281')

        self.__setup_characteristic = controller_setup_data_characteristic
        self.__sensor_characteristic = controller_data_characteristic

        self.write(bytearray(b'\x01\x00'), 3)
        self.write(bytearray(b'\x06\x00'), 1)
        self.write(bytearray(b'\x07\x00'), 1)
        self.write(bytearray(b'\x08\x00'), 3)

        self.__reset = self.__volbtn = self.__trig = True
        self.__time = round(time.time()) + 10
        self.__lastupdated = 0
        self.__updatecounts = 0
        self.__VR = False

        controller_data_characteristic.enable_notifications()
        print("Готов")

    def keepalive(self):
        if (time.time() > self.__time):
            self.__time = round(time.time()) + 10
            cmd = bytearray(b'\x04\x00')
            for i in range(4):
                self.__setup_characteristic.write_value(cmd)

    def characteristic_value_updated(self, characteristic, value):
        if (characteristic == self.__sensor_characteristic):
            if self.__VR == False:
                self.__updatecounts += 1
                if self.__updatecounts == 20:
                    now = time.time()
                    self.__updatecounts = 0
                    deltatime = now - self.__lastupdated
                    self.__lastupdated = now
                    if deltatime > 0.23:
                        self.write(bytearray(b'\x06\x00'), 1)
                        self.write(bytearray(b'\x08\x00'), 3)
                        self.write(bytearray(b'\x07\x00'), 1)
                    else:
                        print("VR mode enabled ", deltatime)
                        self.__VR = True

            self.keepalive()
            int_values = [x for x in value]
            if (len(int_values) < 60):
                self.__VR = True
                print("VR mode is activated")
                self.write(bytearray(b'\x01\x00'), 3)
                self.__sensor_characteristic.enable_notifications()
                return

            triggerButton    = True if ((int_values[58] &  1) ==  1) else False
            homeButton       = True if ((int_values[58] &  2) ==  2) else False
            backButton       = True if ((int_values[58] &  4) ==  4) else False
            volumeUpButton   = True if ((int_values[58] & 16) == 16) else False
            volumeDownButton = True if ((int_values[58] & 32) == 32) else False
            NoButton         = True if ((int_values[58] & 64) == 64) else False

            if (triggerButton == True and self.__trig == True):
                self.__trig = False
                print("Кнопка 4")
                return

            if (homeButton == True and self.__volbtn == True) :
                self.__volbtn = False
                print("Кнопка 2")
                return

            if (backButton == True and self.__volbtn == True):
                self.__volbtn = False
                print("Кнопка 1")
                requests.post("http://localhost/api/click",{'click':'1'})
                return

            if (volumeDownButton == True and self.__volbtn == True):
                self.__volbtn = False
                print("Звук вниз")
                return

            if (volumeUpButton == True and self.__volbtn == True):
                self.__volbtn = False
                print("Звук вврех")
                return

            if (NoButton == True):
                self.__volbtn = True
                self.__trig = True
                return
def defint():
    global device
    sys.exit(0)

print("Samsung Gear VR ...")

device = AnyDevice(mac_address='2C:BA:BA:5E:DE:60', manager=manager)
device.connect()

manager.run()
