Gem::Specification.new do |s|
  s.name = 'bluebutton'
  s.email = 'site4@mail.ru'
  s.summary = 'Deal with Bluetooth button aka selfy shutter'
  s.description = <<EOF
Simple daemon that allows you to execute action when bluetooth button shutter pressed. So you can control your PC by low energy button device and few scripts.
EOF
  s.authors = ["Alex Davidov"]
  s.homepage = 'https://bitbucket.org/site4yo/bluebutton'
  s.license = 'FREE BSD'
  s.files = %w(
README.md
lib/bluebutton.rb
bin/bluebutton
VERSION
)
  s.executables = ['bluebutton']

  s.required_ruby_version = "~> 2"
  s.add_runtime_dependency "device_input"
  s.add_runtime_dependency "slop"
  s.add_runtime_dependency "unirest"

  s.version = File.read(File.join(__dir__, 'VERSION')).chomp
end
